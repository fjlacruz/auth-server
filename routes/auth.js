const { Router } = require("express");
const { check } = require("express-validator");
const { crearUsuario, login, renew, getUsuarios, getUsuariosById, nuevoUsuario, updateUS, deleteUs } = require("../controllers/auth");
const { validarCampos } = require("../middlewares/validarCampos");
const { validarJWT } = require('../middlewares/validar-jwt');

const router = Router();

router.post('/new', [
    check('name', 'Campo requerido').not().isEmpty(),
    check('email', 'Campo requerido').isEmail(),
    check('password', 'Campo requerido').isLength({ min: 6 }),
    validarCampos
], crearUsuario);

router.post('/', [
    check('email', 'Campo requerido').isEmail(),
    check('password', 'Campo requerido').isLength({ min: 6 }),
    validarCampos
], login);

router.get('/renew', validarJWT, renew);
router.get('/getUsuarios/', getUsuarios);
router.get('/getUsuariosById/:id', getUsuariosById);
router.post('/nuevoUsuario/', nuevoUsuario);
router.put('/updateUS/:id', updateUS);
router.delete('/deleteUs/:id', deleteUs);



module.exports = router;