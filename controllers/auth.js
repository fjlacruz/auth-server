
const { response } = require("express");
require('dotenv').config();
const Usuario = require('../models/Usuario');
const bcrypt = require("bcryptjs");
const { generarJWT } = require("../helpers/jwt");


//============ conexion a postgres ============//
var connPostgres = require("../DB/configPostgres");
const { Pool } = require('pg');
const pg = new Pool(connPostgres)
//=============================================//


const getUsuarios = async (req, res) => {
    try {
        const response = await pg.query('SELECT * FROM table_test ORDER BY id ASC');
        res.status(200).json(response.rows);
    } catch (error) {
        console.log(error.message);
        res.status(200).json({
            ok: false,
            msg: error.message
        });
    }
};


const getUsuariosById = async (req, res) => {
    try {
        console.log(req.params.id);
        let id = req.params.id;
        const response = await pg.query(`SELECT * FROM table_test WHERE id = ${id}`);
        res.status(200).json({
            id: response.rows[0].id,
            name: response.rows[0].name,
            email: response.rows[0].email
        });
    } catch (error) {
        console.log(error.message);
        res.status(200).json({
            ok: false,
            msg: error.message
        });
    }
};

const nuevoUsuario = async (req, res) => {

    try {
        const { name, email } = req.body;

        const valEmail = await pg.query('SELECT email FROM table_test where email = $1', [email]);
        //console.log('valEmail: ', valEmail.rowCount);
        if (valEmail.rowCount >= 1) {
            res.status(200).json({
                ok: false,
                msg: 'el email existe'
            });
        } else {
            const response = await pg.query(`INSERT INTO table_test (name, email) VALUES ($1, $2)`, [name, email]);
            res.status(200).json({
                ok: true,
                data: {
                    user: { name, email }
                }
            })
        }
    } catch (error) {
        console.log(error.message);
        res.status(200).json({
            ok: false,
            msg: error.message
        });
    }
}

const updateUS = async (req, res) => {
    try {
        const id = parseInt(req.params.id);
        const { name, email } = req.body;

        const response = await pg.query('UPDATE table_test SET name = $1, email = $2 WHERE id = $3', [
            name,
            email,
            id
        ]);
        res.status(200).json({
            ok: true,
            msg: 'Usuario actualizado'
        }
        );
    } catch (error) {
        console.log(error.message);
        res.status(200).json({
            ok: false,
            msg: error.message
        });
    }
}


const deleteUs = async (req, res) => {
    try {
        const id = parseInt(req.params.id);
        await pg.query('DELETE FROM table_test where id = $1', [
            id
        ]);
        res.status(200).json(`User ${id} Eliminado`);

    } catch (error) {
        console.log(error.message);
        res.status(200).json({
            ok: false,
            msg: error.message
        });

    }
}


const crearUsuario = async (req, res = response) => {
    //console.log('req: ', req.body);
    const { name, email, password } = req.body
    console.log(name, email, password);

    try {
        //verificar email
        const usuario = await Usuario.findOne({ email })

        if (usuario) {
            return res.status(400).json({
                ok: false,
                msg: 'ya existe email'
            })
        }
        //crear usuario con el modelo
        dbUser = new Usuario(req.body);

        //hash de la contraseña
        const salt = bcrypt.genSaltSync();
        dbUser.password = bcrypt.hashSync(password, salt);

        // Generar el TOKEN - JWT
        const token = await generarJWT(dbUser._id);

        //crear usuario de DB
        await dbUser.save();
        return res.status(201).json({
            ok: true,
            uid: dbUser._id,
            name,
            token
        })

        //generar respuesta exitosa
    } catch (error) {
        console.log(error);

        return res.status(500).json({
            ok: true,
            msg: 'Comuniquese con el adminsitrador'
        })
    }

}

const login = async (req, res = response) => {

    const { email, password } = req.body
    console.log(email, password);

    try {
        // Verificar email
        const dbUser = await Usuario.findOne({ email });

        if (!dbUser) {
            return res.status(404).json({
                ok: false,
                msg: "Email no encontrado",
            });
        }
        // Verificar contraseña
        const validPassword = bcrypt.compareSync(password, dbUser.password);
        if (!validPassword) {
            return res.status(400).json({
                ok: false,
                msg: "Contraseña no válida",
            });
        }
        // Generar el TOKEN - JWT
        const token = await generarJWT(dbUser._id);

        return res.json({
            ok: true,
            uid: dbUser._id,
            token
        });


    } catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            msg: 'Hable con el Admin'
        })

    }

}
const renew = async (req, res = response) => {

    const { uid } = req;
    console.log(uid);


    // Generar el TOKEN - JWT
    const token = await generarJWT(uid);

    // Obtener el usuario por UID
    const usuario = await Usuario.findById(uid);

    res.json({
        ok: true,
        token,
        usuario
    });


}

module.exports = {
    crearUsuario,
    login,
    renew,
    getUsuarios,
    getUsuariosById,
    nuevoUsuario,
    updateUS,
    deleteUs
};



