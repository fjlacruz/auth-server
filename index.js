const express = require('express');
const cors = require('cors');
const { dbConnection } = require("./DB/config");

const app = express();

//Base de datos
dbConnection();

//Directorio Publico
app.use(express.static("public"));

//CORS
app.use(cors());

//lectura y parseo de body
app.use(express.json());

//Rutas
app.use('/api/auth', require('./routes/auth'));

app.listen(4000, () => {
    console.log(`Servidor corriendo en puerto ${process.env.PORT}`);
})



